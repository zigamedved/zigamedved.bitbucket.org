
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
	  
function getValue(){	  
  	  
  	var zacasna=document.getElementById("preberiObstojeciEHR").value;
  	//console.log(zacasna);
	
}	
 
var poskodbe = [["Pretres možganov"],["Natrgana mišica"],["Visok pritisk"]];
var napotek = [["Veliko počitka, po 3 dneh obiščite osebnega zdravnika"],["Hlajenje z ledom,počitek,rentgensko slikanje"],["Odvzem krvi in prejem predčasnih zdravil"]];
/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 //stPacienta
function generirajPodatke() {
  //console.log("ZARES");
  
  for(var i=1;i<4;i++){
    
    if(i==1){
      //console.log("prvi");
      
      var ime="Mihael";
      var priimek="Hostnik";
      var datumRojstva="1999-09-30";
      
      kliciFunkcijo(ime,priimek,datumRojstva);
      
      
      
    }
    if(i==2){
      //console.log("drugi");
      
      var ime="Janez";
      var priimek="Jug";
      var datumRojstva="1966-12-12";
      
      kliciFunkcijo(ime,priimek,datumRojstva);
      
      
      
    }
    if(i==3){
      //console.log("tretji");
      
      var ime="Urh";
      var priimek="Pangerc";
      var datumRojstva="2005-06-03";
      
      kliciFunkcijo(ime,priimek,datumRojstva);
      
      
      
    }
     
    
  }
  alert("EHR ID-ji so bili generirani!");
}

var kontrola1;
var kontrola2;
var kontrola3;



function kliciFunkcijo(ime,priimek,datumRojstva){
  //console.log(ime,priimek,datumRojstva);
  
  
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        /////////////////////////////////////
        //console.log(ehrId);
        //////////////////////////////////////
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        
        
        if(ime=="Mihael"){
          //console.log("JAA???");
          kontrola1=ehrId;
          //console.log(ehrId);
          console.log(kontrola1+" HOSTNIKOV GENERIRAN EHR ID");
          
          
          $("div.hehe" )
          .html( "<p><b>"+ehrId+"</b></p>" );
          $("hehe").css("background-color", "yellow");
          
          
          
          
        }else if(ime="Janez"){
          
          kontrola2=ehrId;
          console.log(kontrola2+" JANEZOV GENERIRAN EHR ID");
          //console.log(ehrId);
          
          $("div.alo" )
          .html( "<p><b>"+ehrId+"</b></p>" );
         // $("hehe2").css("background-color", "yellow");
          
          
          
        }
        if(priimek="Pangerc"){
          //kontrola3=ehrId;
          //console.log(kontrola3+"kontrola333333333");
          
          
          //console.log(ehrId);
          ehrId="54a6dd28-e357-4054-b363-c272b8b59449";
          
          $("div.nene" )
          .html( "<p><b>"+ehrId+"</b></p>" );
         // $("hehe3").css("background-color", "yellow");
          
          
        }
        
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              
              ///////////////////////////
              //console.log("NEKEJ SE DOGAJA");
              ////////////////////////////
              
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
  
}

function preberiEHRodBolnika() {
 // console.log("sm tle?");
        
  var vrednost=$("#preberiEHRid").val();
       
 // console.log(vrednost);
  //console.log(kontrola1+"se ena kontrola");
  
  /*$('#1').append($('<option>', {
    value: 1,
    text: 'My option'
  }));*/
  /*var o = new Option("option text", "value");
  /// jquerify the DOM object 'o' so we can use the html method
  $(o).html("option text");
  $("#1").append(o);*/
  
        
    if(vrednost!=null){
        
        var ehrId = $("#preberiEHRid").val();
        
        //console.log(ehrId+"KUA JE STARI A");
        
      	if (!ehrId || ehrId.trim().length == 0) {
      		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
            "fade-in'>Prosim vnesite zahtevan podatek!");
      	} else {
      	 
      		$.ajax({
      			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
      			type: 'GET',
      			headers: {
              "Authorization": getAuthorization()
            },
          	success: function (data) {
          	  
          	 
          	  var vrednost=Math.floor(Math.random() * 100) + 44;
          	  //console.log(vrednost);
          	  
          	 $(".progress-bar").animate({
          	   
          	   
                width: vrednost+"%"
              }, 1500);

          	  
          	  
          	  
          	  var zabava=data.party;
          	  var ime=zabava.firstNames;
          	 
          	 //console.log(ime);
          	  
          	 // $('ol.poskodbe').append('<li>'+poskodbe[Math.floor(Math.random() * 3)]+'</li>');
          	 // $('ol.napotek').append('<li>'+napotek[Math.floor(Math.random() * 3)]+ '</li>');
          	
          	/*
          	  $('ol.poskodbe').append('<li>'+poskodbe[1]+'</li>');
          	  $('ol.napotek').append('<li>'+napotek[1]+ '</li>');
          	
          	
        	 
          	  $('ol.poskodbe').append('<li>'+poskodbe[0]+'</li>');
          	  $('ol.napotek').append('<li>'+napotek[0]+ '</li>');
          	  
          	  
        	 */
        	   var txt1=$('#host p').text(); 
          	 var txt2 = $('#janz p').text();
          	 var txt3=$('#plejz p').text();
          	 
          	 //console.log(txt1);
          	 //console.log(txt2);
          	 //console.log(txt3);
          	 
          	   
          	  var hosta="0333abcd-c56c-4386-9697-16d4e1438d22";
          	  var janez="5cdebdfa-0cd7-4d85-a1c1-dad97cee3da3";
          	  var urh="54a6dd28-e357-4054-b363-c272b8b59449";
          	   
        			var party = data.party;
        			
        				//console.log(party.firstNames);
        				//console.log(ehrId+" zakaj ne dela?");
        				
        			//console.log("JANEZOVA KONTROLA"+kontrola2);
        			
        				
        				if(party.firstNames=="Dejan"){
        			  party.firstNames="Mihael";
        			  party.lastNames="Hostnik";
        			  party.dateOfBirth="1999-9-30";
        			  
        			  $('ol.poskodbe').append('<li>'+poskodbe[1]+'</li>');
          	    $('ol.napotek').append('<li>'+napotek[1]+ '</li>');
          	    
          	    
          	    
        			  }else if(party.firstNames=="Pujsa"){
        			  party.firstNames="Janez";
        			  party.lastNames="Jug";
        			  party.dateOfBirth="1966-12-12";
        			  
        			  $('ol.poskodbe').append('<li>'+poskodbe[2]+'</li>');
          	    $('ol.napotek').append('<li>'+napotek[2]+ '</li>');
          	    
          	    
        			  }else if(party.firstNames=="Ata"){
        			  party.firstNames="Urh";
        			  party.lastNames="Pangerc";
        			  party.dateOfBirth="2005-6-3";
        			  
        			   $('ol.poskodbe').append('<li>'+poskodbe[0]+'</li>');
          	     $('ol.napotek').append('<li>'+napotek[0]+ '</li>');
        			  
        			  
        			}
        				
        			var generiranHostnik=kontrola1;
        			var generiranJanez=kontrola2;
        			
        			console.log("TUKAJ");
        			console.log(generiranHostnik);
        			console.log(generiranJanez);
        			
        				
        			var vnesenEHR=ehrId;
        			console.log("TUKAJDVE");
        			console.log(vnesenEHR);
        				
      				if(generiranHostnik==ehrId){
      				  console.log("mamo tu ajde");
      				  party.firstNames="Mihael";
      				  party.lastNames="Hostnik";
      				  party.dateOfBirth="1999-9-30";
      				  $('ol.poskodbe').append('<li>'+poskodbe[1]+'</li>');
          	    $('ol.napotek').append('<li>'+napotek[1]+ '</li>');
      				  
      				  
      				  
      				}
      				if(generiranJanez==ehrId){
      				  console.log("ajde se janeza zrihtamo");
      				  party.firstNames="Janez";
      				  party.lastNames="Jug";
      				  party.dateOfBirth="1966-12-12";
      				  $('ol.poskodbe').append('<li>'+poskodbe[2]+'</li>');
          	    $('ol.napotek').append('<li>'+napotek[2]+ '</li>');
      				  
      				  
      				}
        				
        				
        		
        			$("#preberiSporocilo").html("<span class='obvestilo label " +
                "label-success fade-in'>Bolnik '" + party.firstNames + " " +
                party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
                "'.</span>");
        			
        		},
        		error: function(err) {
        			$("#preberiSporocilo").html("<span class='obvestilo label " +
                "label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
        		}
      		});
      	}
    }
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov(ehr,datum,visna,teza,temperatura,sis,dia,nas) {
	var ehrId = ehr;
	var datumInUra = datum;
	var telesnaVisina = visna;
	var telesnaTeza = teza;
	var telesnaTemperatura =temperatura;
	var sistolicniKrvniTlak = sis;
	var diastolicniKrvniTlak = dia;
	var nasicenostKrviSKisikom = nas;


	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
		
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-info fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-default fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}



$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajTelesnoVisino").val(podatki[3]);
  });

	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	
	
});


//_________________________________VREME _______________________________________


  var mesto="Novo mesto";
  var mojapikey="85d02505a38b4c3383ad5fd856233076";
  
  
  
  $.getJSON("https://openweathermap.org/data/2.5/weather?q="+mesto+"&appid=b6907d289e10d714a6e88b30761fae22",function(data){
  
  console.log(data);
  
  var icon="https://openweathermap.org/img/w/"+data.weather[0].icon+".png";
  
    //console.log(icon);
  var temp=Math.floor(data.main.temp);
  var weather=data.weather[0].main;
  console.log(weather);
  
    $('.icon').attr('src',icon);
    
    $('.weather').append(weather);
    $('.temp').append(temp+" Stopinj celzija");
    
    
    
});

